@extends('layout/main')



@section('title', 'Dashboard')
@section('bread', 'Dashboard')

@section('section')
<!-- Main content -->
<!-- Section() untuk HTML sedangkan Stack() untuk script java
 untuk meletakan layout sesuai kebutuhan
 menandakan dia akan mengambil data dari folder views -> layout -> main 
section = apa pun yang di tuliskan pada yeild
// jika section() lebih dari satu baris maka di akhiri dengan endsection
// jika ada section yang di dalamnya yeild('section') maka tampilkan isi sectionnya
-->
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col" style="width: 10px">#</th>
                        <th scope="col">Nama Lengkap</th>
                        <th scope="col">Email</th>
                        <th scope="col">Photo</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($profil as $p)
                    <tr>
                        <!-- $loop->iteration = interasi looping saat ini di mulai dari 1, kalau di php kita ingin memberikan no 1 harus menambahkan $no = 1 -->
                        <th scope="row">{{ $loop->iteration}}</th>
                        <td>{{ $p->nama_lengkap}}</td>
                        <td>{{ $p->email}}</td>
                        <td>{{ $p->foto}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <!-- ./col -->

            <!-- ./col -->

            <!-- ./col -->
        </div>

</section>
@endsection()
<!-- /.Left col -->
<!-- right col (We are only adding the ID to make the widgets sortable)-->