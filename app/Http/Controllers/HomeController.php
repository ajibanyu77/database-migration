<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function home()
    {
        $profil = DB::table('profil')->get(); // SELECT * FROM profil
        // dd($user);
        return view('index', ['profil' => $profil]);
    }
}
